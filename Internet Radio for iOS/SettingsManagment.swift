//
//  SettingsManagment.swift
//  Internet Radio for iOS
//
//  Created by Felix v. Oertzen on 08.07.19.
//  Copyright © 2019 Felix v. Oertzen. All rights reserved.
//
/*
 Internet Radio for iOS
 Copyright (C) 2019  Felix v. O.
 felix@von-oertzen-berlin.de
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

import Foundation

class SettingsManagment {
    let KEY_MOBILE_DATA_ENABLED     = "MOBILE_DATA_ENABLED";
    let KEY_RETRY_PLAYBACK          = "RETRY_PLAYBACK";
    
    let defaults: UserDefaults;
    
    init () {
        defaults = UserDefaults.standard
    }
    
    func isMobileDataEnabled() -> Bool {
        defaults.synchronize()
        return defaults.bool(forKey: "");
    }
    
    func setMobileDataEnabled(isEnabled enabled:Bool) {
        defaults.set(enabled, forKey: KEY_MOBILE_DATA_ENABLED);
        defaults.synchronize();
    }
    
    func isPlaybackRetryEnabled() -> Bool {
        defaults.synchronize()
        return defaults.bool(forKey: KEY_RETRY_PLAYBACK);
    }
    
    func setRetryPlaybackEnabled(isEnabled enabled:Bool) {
        defaults.set(enabled, forKey: KEY_RETRY_PLAYBACK);
    }
}
