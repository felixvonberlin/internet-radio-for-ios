//
//  FirstViewController.swift
//  Internet Radio for iOS
//
//  Created by Felix v. Oertzen on 26.03.19.
//  Copyright © 2019 Felix v. Oertzen. All rights reserved.
//

/*
 Internet Radio for iOS
 Copyright (C) 2019  Felix v. O.
 felix@von-oertzen-berlin.de
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


import UIKit
import AVFoundation
import MediaPlayer

class FavoritesController: UIViewController {
    
    @IBOutlet weak var mainTable: UITableView!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var stationDescr: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    
    private var radioManagment: StationManagment!;
    private var settings: SettingsManagment!;
    private var audioPlayer: AVPlayer!;
    private var playerItem: AVPlayerItem!
    
    private var playingStation: RadioStation!;
    private var reachability: Reachability?;
    

    @IBAction func editTouched(_ sender: Any) {
        mainTable.setEditing(!mainTable.isEditing, animated: true)
        editButton.title = mainTable.isEditing ? NSLocalizedString("DONE", comment: "") : NSLocalizedString("EDIT", comment: "");
        editButton.style = mainTable.isEditing ? UIBarButtonItem.Style.done : UIBarButtonItem.Style.plain;
    }
    
    @IBAction func playPauseTouched(_ sender: Any) {
        if let a = audioPlayer {
            if (a.isPlaying) {
                pause()
            } else {
                play()
            }
        } else {
            playPauseButton.setImage(UIImage(named: "roundplay"), for: .normal)
            playRow(StationNumber: nil)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let kP = keyPath, kP != "timedMetadata" { return }
        let data: AVPlayerItem = object as! AVPlayerItem
        if let timedData = data.timedMetadata {
            for item in timedData {
                updateTitle(StationName: playingStation.getStationName(), StationsSongTitle: item.value! as! String)
            }
        }
    }
    
    private func play() -> Void {
        if let a = audioPlayer {
            if (a.timeControlStatus == .paused) {
                playPauseButton.setImage(UIImage(named: "roundstop"), for: .normal)
                a.play();
                return;
            }
            if (a.timeControlStatus == .playing) {
                return;
            }
        }
        playPauseButton.setImage(UIImage(named: "roundstop"), for: .normal)
        playRow(StationNumber: nil);
    }
    
    private func pause() -> Void {
        audioPlayer.pause();
        playPauseButton.setImage(UIImage(named: "roundplay"), for: .normal)
    }
    
    
    private func playRow(StationNumber position:Int?) -> Void {
        var selectedStation: Int = 0;
        
        if let selectedRow = mainTable.indexPathForSelectedRow {
            selectedStation = selectedRow[1]
        }
        
        if let pos = position {
            selectedStation = pos;
        }
        
        if (radioManagment.getStations().count > selectedStation) {
            let station = radioManagment.getStations()[selectedStation]
            play(Station: station)
            playingStation = station;
            mainTable.selectRow(at: IndexPath.init(row: selectedStation, section: 0), animated: true, scrollPosition: UITableView.ScrollPosition.middle)
        } else {
            let alertController = UIAlertController(title:  NSLocalizedString("ERRORNOSTATION", comment: ""), message: NSLocalizedString("ERRORNOSTATIONDESCRIPTION", comment: ""), preferredStyle: .alert)
            
            let addAction = UIAlertAction(title: NSLocalizedString("ADDSTATION", comment: ""), style: .default, handler: { (action: UIAlertAction!) in
                if let tabBarController = self.tabBarController {
                    tabBarController.selectedIndex = 1
                }
            })
            let closeAction = UIAlertAction(title: NSLocalizedString("CLOSE", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(addAction)
            alertController.addAction(closeAction)
            present(alertController, animated: true, completion: nil)
            playingStation = nil;
        }
    }
    
    private func play(Station station:RadioStation) -> Void {
        if let r = reachability,
            r.connection == Reachability.Connection.unavailable {
            showNetErrorDialog()
            return
        }
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        try! AVAudioSession.sharedInstance().setActive(true)
        
        playerItem = AVPlayerItem(url: station.getStationUrl())
        playerItem.addObserver(self, forKeyPath: "timedMetadata", options: NSKeyValueObservingOptions(), context: nil)
        
        audioPlayer = AVPlayer(playerItem: playerItem)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemFailedToPlay(_:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
        audioPlayer.play()
        
        updateTitle(StationName: station.getStationName(), StationsSongTitle: station.getStationUrlHumanReadable())
        playPauseButton.setImage(UIImage(named: "roundstop"), for: .normal)
        setupRemoteTransportControls()
    }
    
    @objc func playerItemFailedToPlay(_ notification: Notification) {
        if notification.name as NSString == "AVPlayerItemPlaybackStalledNotification" {
            debugPrint("Network Changed")
        }
    }
    
    func updateTitle(StationName station:String, StationsSongTitle desc:String) {
        var nowPlayingInfo = [String : Any]()
        let s = desc.isEmpty ? station : desc;
        let d = desc.isEmpty ? "" : station;
        
        nowPlayingInfo[MPMediaItemPropertyTitle] = s
        nowPlayingInfo[MPMediaItemPropertyArtist] = d
        
        stationName.text = d
        stationDescr.text = s
        playPauseButton.setImage(UIImage(named: audioPlayer.isPlaying ? "roundstop" : "roundplay"), for: .normal)
        
        
        if let image = UIImage(named: "lockscreen") {
            nowPlayingInfo[MPMediaItemPropertyArtwork] =
                MPMediaItemArtwork(boundsSize: image.size) { size in
                    return image
            }
        }
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = 0
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = 0
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = audioPlayer.rate
        nowPlayingInfo[MPNowPlayingInfoPropertyIsLiveStream] = true
        
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }

    
    override func viewDidLoad() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        radioManagment = StationManagment();
        mainTable.delegate = self;
        mainTable.dataSource = self;
        
        mainTable.register(UITableViewCell.self, forCellReuseIdentifier: "recyclingIdentifier")
//        radioManagment.addStation(station: (RadioStation(name: "Bayern 3", url: "http://streams.br.de/bayern3_2.m3u")));
//        radioManagment.addStation(station: (RadioStation(name: "104.6 RTL", url: "http://stream.104.6rtl.com/rtl-live/mp3-192/listenlive/play.m3u")));
//        radioManagment.addStation(station: (RadioStation(name: "JAM FM", url: "http://www.jam.fm/streams/jam-nmr-mp3.m3u")));
//        radioManagment.addStation(station: (RadioStation(name: "Deutschland Funk", url: "http://www.listenlive.eu/streams/germany/de_dlf_128.m3u")));
//        radioManagment.addStation(station: (RadioStation(name: "Energy", url: "http://www.listenlive.eu/streams/germany/energyberlin.m3u")));

        settings = SettingsManagment()
        radioManagment.loadStations()
        
        let reachability: Reachability?
        reachability = try? Reachability()
        self.reachability = reachability
        
        startMonitoring()
        try? reachability?.startNotifier()
        super.viewDidLoad();
        self.navigationItem.largeTitleDisplayMode = .always
    }
    
    /// Starts monitoring the network availability status
    func startMonitoring() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reachabilityChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
    }

    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .none:
            ()
        case .wifi:
            ()
        case .cellular:
            if let player = audioPlayer {
                if (player.isPlaying && !settings.isMobileDataEnabled()) {
                    pause();
                    let alertController = UIAlertController(title: NSLocalizedString("DIALOG_NETWORK_ERROR", comment: ""), message: NSLocalizedString("DIALOG_WIFI_LEFT_DESCRIPTION", comment: ""), preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: NSLocalizedString("GOTIT", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    present(alertController, animated: true, completion: nil)
                }
            }
        case .unavailable:
            if let _ = audioPlayer {
                pause();
            }
            showNetErrorDialog();
        }
    }
    
    func showNetErrorDialog() {
        let alertController = UIAlertController(title: NSLocalizedString("DIALOG_NETWORK_ERROR", comment: ""), message: NSLocalizedString("DIALOG_NETWORK_LEFT_DESCRIPTION", comment: ""), preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("CLOSE", comment: ""), style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OPEN_SETTINGS", comment: ""), style: .cancel, handler: { (action: UIAlertAction!) in
            if let url = URL(string:"App-prefs:root=WIFI") {
                if UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }))
        present(alertController, animated: true, completion: nil)
    }
    
    func stopNotifier() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    deinit {
        stopNotifier()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        radioManagment.loadStations()
        mainTable.reloadData()
    }
    
    func setupRemoteTransportControls() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()
        UIApplication.shared.beginReceivingRemoteControlEvents()

        commandCenter.playCommand.addTarget { [unowned self] event in
            if self.audioPlayer.rate == 0.0 {
                self.play()
                return .success
            }
            return .commandFailed
        }
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            if self.audioPlayer.rate == 1.0 {
                self.pause()
                return .success
            }
            return .commandFailed
        }
        commandCenter.nextTrackCommand.addTarget { [unowned self] event in
            if let actualStation = self.playingStation {
                if let actualPosition = self.radioManagment.getStations().firstIndex(where: {$0 == actualStation}) {
                    self.playRow(StationNumber: actualPosition + 1 < self.radioManagment.getStations().count ? actualPosition + 1 : 0)
                }
            }
            return .success
        }
        commandCenter.previousTrackCommand.addTarget { [unowned self] event in
            if let actualStation = self.playingStation {
                if let actualPosition = self.radioManagment.getStations().firstIndex(where: {$0 == actualStation}) {
                    self.playRow(StationNumber: (actualPosition - 1 >= 0 ? actualPosition - 1 : self.radioManagment.getStations().count - 1))
                }
            }
            return .success
        }
    }
}

extension FavoritesController:UITableViewDataSource {
    // delegate functions for table view
    private func numberOfSections(in tableview: UITableViewCell) -> Int {
        return 1;
    }
    
    func tableView(_ tableview: UITableView, numberOfRowsInSection section:Int) -> Int {
        return section == 0  ? radioManagment.getStations().count : 0;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.radioManagment.deleteStation(position: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.radioManagment.saveStations()
        }
    }
    
    // Determine whether a given row is eligible for reordering or not.
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath){
        self.radioManagment.moveStation(From: sourceIndexPath.row, To: destinationIndexPath.row)
        self.radioManagment.saveStations()
    }
    
    func tableView(_ tableview: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "recyclingIdentifier")

        cell.textLabel!.text = (radioManagment.getStations()[indexPath.row]).getStationName();
        cell.detailTextLabel?.text = (radioManagment.getStations()[indexPath.row]).getStationUrlHumanReadable();
        return cell;
    }
}

extension FavoritesController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        playRow(StationNumber: nil)
    }
}

extension AVPlayer {
    var isPlaying: Bool { return (timeControlStatus == .playing) && rate != 0 && error == nil; }
}

